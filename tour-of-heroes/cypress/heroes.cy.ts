import { HeroesComponent } from "src/app/heroes/heroes.component"

describe('heroes.cy.ts', () => {
  it('mounts', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  });
    cy.mount(HeroesComponent)
    
  })
})