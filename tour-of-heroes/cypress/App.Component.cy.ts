import { AppComponent } from "src/app/app.component";


describe('AppComponent', () => {
  it('mounts', () => {
    Cypress.on('uncaught:exception', (_err, _runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  });
  cy.mount(AppComponent)
  })
})
