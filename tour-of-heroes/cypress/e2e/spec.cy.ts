describe('template spec', () => {
  it('visits the production site', () => {
    cy.visit('https://myapp-ng.web.app')
  });

  it('visits the staging site', () => {
    cy.visit('https://myapp-ng-stg.web.app')
  });

  //test global routing
  it('shows the default route which is the dashboard when first loaded', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  });
    cy.visit('http://localhost:39079/');

    cy.contains('Dashboard').click();
    
    cy.url().should('include', '/dashboard');

  })

  it('heroes tab should be clickable, url should change', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  });
  cy.visit('http://localhost:39079/');
  
  cy.get(':nth-child(1) > .nav-link > h4').click();
  cy.url().should('include', '/heroes');


  })  

  it('clicking on a hero navigates to the parametized route', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
      // returning false here prevents Cypress from
      // failing the test
      return false
  });
  cy.visit('http://localhost:39079/');

  cy.get('[ng-reflect-router-link="/detail/13"]').click();

  cy.url().should('include', '/detail/13');  

  //clicking go back should take us back to the dashboard

  cy.get('.header.section-header > :nth-child(3)').click();

  cy.url().should('include', '/dashboard');

  })

  
  //end test gloabal routing



})