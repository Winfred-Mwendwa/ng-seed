// Server that serializes and deserializes JSON to JSONAPI format
var toJsonApiObject = function (objType, obj) {
  let { id, ...attributes } = obj;
  return {
    id: id,
    type: objType,
    attributes: attributes,
  };
};

var jsonapiWrapper = (resource, jsonPayload) => {
  if (Array.isArray(jsonPayload)) {
    jsonapiBody = jsonPayload.map((p) => toJsonApiObject(resource, p));
  } else {
    jsonapiBody = toJsonApiObject(resource, jsonPayload);
  }
  return jsonapiBody;
};

const jsonServer = require("json-server");
const server = jsonServer.create();
const path = require("path");
const router = jsonServer.router(path.join(__dirname, "db.json"));
const middlewares = jsonServer.defaults();
const port = process.env.PORT || 3000;

server.use(middlewares);
server.listen(port, () => {
  console.log(`JSON Server is running: http://localhost:${port}`);
});

router.render = (req, res) => {
  // console.log(req.resource)
  resource = req.path.match(/^\/([^\/]*)/)[1];
  res.jsonp({
    data: jsonapiWrapper(resource, res.locals.data),
  });
};

server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
  // console.log(req.body.data)

  if (["POST", "PATCH", "PUT"].includes(req.method)) {
    req.body = req.body.data.attributes;
  }
  next();
});

server.use(router);
